# ECE 358 Lab 1
This readme will show how to run the code using the provided Makefile.

## Setup
If not running on the eceubuntu server, certain imports need to be installed. This can be done through the use of the command `make setup`.
It can also be done manually by performing the following commands in the terminal:

```
pip install -r numpy
pip install -U matplotlib
```

## Running the Code
### Question 1
To run the code for question one, use the command `make q1`.
```
make q1
```
### Question 2
The code for question two can be ran using one of the following commands for question three or four. Each one was made with slight alterations based on the specified ρ for each questions. 
### Question 3
To run the code for question three, use the command `make q3`.
```
make q3
```
### Question 4
To run the code for question four, use the command `make q4`.
```
make q4
```
### Question 5
To run the code for question five, use the command `make q5`.
```
make q5
```

## Clean Up
After running the code, you can use the command `make clean` to clean up. This will remove any png files that were previously created by the simulations.
```
make clean
```
